package com.arun.aashu.popupmenu;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        findViewById(R.id.btn1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(MainActivity.this,Main2Activity.class));


            }
        });


        findViewById(R.id.btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu p=new PopupMenu(MainActivity.this,findViewById(R.id.btn));
                p.getMenuInflater().inflate(R.menu.popup_menu,p.getMenu());

                p.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {


                        switch (menuItem.getItemId()){

                            case R.id.item1:
                                findViewById(R.id.btn).setBackgroundColor(Color.RED);
                                break;
                            case R.id.item3:
                                findViewById(R.id.btn).setBackgroundColor(Color.GREEN);
                                break;
                            case R.id.item2:
                                    findViewById(R.id.btn).setBackgroundColor(getResources().getColor(R.color.colorAccent));
                                    break;

                        }

//                        Toast.makeText(MainActivity.this, "hello", Toast.LENGTH_SHORT).show();



                        return false;
                    }
                });


                p.show();
            }
        });


    }


}
