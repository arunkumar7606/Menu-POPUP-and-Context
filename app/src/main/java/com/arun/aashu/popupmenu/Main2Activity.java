package com.arun.aashu.popupmenu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {
    ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        listView=findViewById(R.id.listview1);

     String data[]= getResources().getStringArray(R.array.abc);
        ArrayAdapter<String> ad=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,data);

        listView.setAdapter(ad);

        registerForContextMenu(listView);

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        menu.add("delete");
        menu.add("Rename");

        super.onCreateContextMenu(menu, v, menuInfo);

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {


        switch (item.getTitle().toString()){
            case "delete":
                Toast.makeText(this, "Item Deleted", Toast.LENGTH_SHORT).show();
                break;
            case "Rename":
                Toast.makeText(this, "Item Rename", Toast.LENGTH_SHORT).show();
                break;
        }

//        Toast.makeText(this, "hello", Toast.LENGTH_SHORT).show();
        return super.onContextItemSelected(item);
    }
}
